---
layout: work-item
title: "Treasures of the AGO: The Hulk"
link: https://store.steampowered.com/app/2605210/AGO_BRISTOL_1775_From_Warship_to_Prison_Hulk/
client: KRAK XR, Art Gallery of Ontario
role: Production Manager (Phase 1)
when: 2021-2022
where: T﻿oronto, Canada
team: Priam Givord, Gillian McIntyre, Sherry Philips, Karen Vanderborght, et al.
date: 2023-05-10T17:43:17.004Z
categories: creative
tags:
  - media
thumbnail: /assets/uploads/hulk_screen-shot-2021-02-23-at-9.34.42-pm.png
images:
  - image: /assets/uploads/hulk-screen-shot-2021-02-23-at-9.35.00-pm.png
  - image: /assets/uploads/hulkscreen-shot-2021-02-23-at-9.34.25-pm.png
---
 The first proof-of-concept VR pilot to establish a museum standard for narration and immersive viewing in virtual reality. Presentation of the AGO’s permanent tall ships collection in virtual reality.\
\
A multi-player, interactive narrative allows visitors and online viewers to immerse themselves in the history of the time and discover details usually hidden within the object on display. The VR format engages through active learning and stimulated imagination.