---
layout: work-item
title: "A-Site "
link: ""
client: KRAK XR
role: Co-Producer
when: "2020"
partners: Karen Vanderborght, Mandy Lam, David Psutka, et al.
date: 2023-05-10T17:47:19.963Z
categories: creative
tags:
  - media
  - project_development
thumbnail: /assets/uploads/screenshot-2023-05-10-at-10.54.12-am.png
images: []
---
With Creative Director, developed curriculum, curated team, generated funding for this interactive, multi-platform training toolkit for artists.

A-site’s hands-on workshops and interactive toolkit series introduce mobile augmented reality and AI as essential contemporary tools in the presentation and dissemination of artwork, and an accessible archive bridging past and future.\
\
“Big tech” offers ubiquitous, accessible tools that abet digital access -- but at costs in areas of autonomy, ethics, and finances - for artist, curator, and public. Do we really want a global conglomerate gatekeeper? Artists and artist-run institutions need their own tools, access, and knowledge to fully benefit from the implementation of AR and AI technologies. The A-site workshop and digital toolkit series put knowledge directly into the hands of artists.\
\
Cutting-edge, custom AR and AI workshops for independent artists and small to mid-range arts organizations: introducing low-cost and free, open-source tools for the presentation and dissemination of multi-disciplinary artistic work.\
\
AR or augmented reality technology creates opportunities around inclusivity, mobility, way-finding, and decision-making — both individual and collaborative. AI needs more input from artists. We explore AI as a co-creator and co-creator. Write with GPT-2, auto-generate music lyrics, compose music, or transform your voice into a violin.\
\
Workshops are targeted to artists, curators, and arts presenters across a full range of disciplines. Topics explore tensions between open-source and models and proprietary concerns, creative constraints and augmentations of AI, and exhibition space-making possibilities of AR.