---
layout: work-item
title: North York Food Security Cluster
client: North York Community House + friends
role: Consultant
date: 2023-05-14T19:56:47.001Z
categories: consulting
tags:
  - sector_development
thumbnail: /assets/uploads/nyscreen-shot-2022-01-02-at-6.08.23-pm.png
images:
  - image: /assets/uploads/nyscreen-shot-2022-01-02-at-6.08.14-pm.png
---
Consultation and writing: C﻿OVID advocacy toolkit