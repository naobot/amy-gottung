---
layout: work-item
title: Long Winter
client: Long Winter Music and Arts Festival
client_link: http://www.torontolongwinter.com/about
role: Executive Director
when: 2016-2020
where: Toronto, Canada
partners: Tea Base Collective, Harbourfront Centre, the Gladstone Hotel, Workman
  Arts, OCAD University, Venus Fest, Bedroomer, Trap Door, Pleasure Dome,
  Trinity Square Video, Lost is Found Collective, The Tranzac, ISO Radio, FSR
  Radio, True Connection, InterAccess, Club Quarantine, et al.
date: 2023-04-04 15:25:07 -0400
categories: creative
tags: festival
thumbnail: /assets/uploads/15895757_903965219739700_4891082244789384885_o.jpg
images:
  - image: /assets/uploads/80586282_1712042755598605_1497086970726187008_o.jpg
  - image: /assets/uploads/lw-vol.1-2015-rcstills.com-89.jpg
  - image: /assets/uploads/lw-vol.1-2015-rcstills.com-42.jpg
  - image: /assets/uploads/77199863_1686365531499661_605811712695730176_o.jpg
  - image: /assets/uploads/82863164_1740642472738633_5126841794822144000_o.jpg
  - image: /assets/uploads/74214484_1712043022265245_1124484143887941632_o.jpg
  - image: /assets/uploads/6r0a4925.jpg
  - image: /assets/uploads/26757026_1140487159420837_3653851233322800587_o.jpg
  - image: /assets/uploads/56367337_1477830342353182_9128998222243561472_o.jpg
  - image: /assets/uploads/48366056_1396698563799694_2819373058882535424_o.jpg
  - image: /assets/uploads/28947436_1176448605824692_4620073392702923378_o.jpg
  - image: /assets/uploads/16422279_921406677995554_1614628805236375241_o.jpg
  - image: /assets/uploads/15937019_903965826406306_5262569916247305934_o.jpg
  - image: /assets/uploads/23675030_1107592906043596_5224116438316570066_o.jpg
---
Toronto's kaleidescopic, carnivalesque all-ages, pay-what-you-can multi-arts series. In four seasons, brought the organization through a critical period of growth: established a first arms-length Board of Directors and strategic planning process, helped formalize collective communication and programming processes; sourced 10+ new venue, program, and presenting partners when the fest lost its home base; initiated first-ever international projects and increased annual revenues and activity by over 400%. Successfully guided the organization into public operating funding and status. Presented over 1,200 artists and engaged audiences of over 20,000. Initiated first international and cross-sector programs, including collaborations with research (Ryerson, Université de Rouens), policy (City of Toronto), and real estate.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Xc56WpiUx3E?si=kF1dwv0tid-Dyzj1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>