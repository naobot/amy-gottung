---
layout: work-item
title: Halocline Trance
link: ""
client: Halocline Trance
client_link: https://haloclinetrance.bandcamp.com/
role: Business Development
when: 2022-present
date: 2023-05-14T20:49:03.851Z
categories: consulting
tags:
  - business_development
  - fundraising
thumbnail: /assets/uploads/screenshot-2024-05-27-at-9.25.58-am.png
images:
  - image: /assets/uploads/htscreenshot-2023-03-13-at-10.48.21-am.png
  - image: /assets/uploads/screenshot-2024-05-27-at-9.28.33-am.png
  - image: /assets/uploads/htscreenshot-2023-03-13-at-10.48.41-am.png
  - image: /assets/uploads/htscreenshot-2023-03-13-at-10.47.57-am.png
---
Ongoing business development for fast-growing avant music label and creative collective.