---
layout: work-item
title: Changeup (hackathon/exhibition)
role: Co-Director
when: September 2018
where: T﻿oronto
team: Brian Wong, Jeff Van Harmelen, Oscar Chiu, Matt Raymond, Sherry Kennedy
partners: T﻿oronto Media Arts Centre, MUTEK (Montreal)
date: 2023-05-10T17:23:38.675Z
categories: creative
tags:
  - hackathon
thumbnail: /assets/uploads/42687404_331982724293814_513709059610771456_o.jpg
images:
  - image: /assets/uploads/42600258_331982700960483_3987281654300278784_o.jpg
  - image: /assets/uploads/42614774_332158774276209_1360716162735800320_o.jpg
  - image: /assets/uploads/42625062_332157980942955_7550486333806870528_o.jpg
  - image: /assets/uploads/42673241_332159467609473_3293455109542903808_o.jpg
  - image: /assets/uploads/42674851_332160334276053_8477873668272160768_o.jpg
  - image: /assets/uploads/42730969_332157610942992_5904651382761193472_o.jpg
  - image: /assets/uploads/42814468_332157720942981_3663116231675740160_o.jpg
  - image: /assets/uploads/20180922_215657.jpg
  - image: /assets/uploads/20180922_200937.jpg
  - image: /assets/uploads/20180922_142043_hdr.jpg
  - image: /assets/uploads/20180922_140756.jpg
  - image: /assets/uploads/20180921_215316.jpg
---
Pop-up tech and art jam, workshop series, and exhibition at [Toronto Media Arts Centre](https://www.tomediaarts.org/). 50+ participants. 3-day creation period. 1 exhibition. 

●○○●●○ MENTORS • PROJECT LEADS ●○○\
\
| FLOATER MENTORS\
●° Markus Heckmann / Derivative / #synth #audio-visual #cg #collaboration #installation\
●° Sarah Friend / <https://isthisa.com> / #blockchain #interactive #generative\
●° Nadine Lessio / #games #tech #design #installation\
●° Protim Roy / #machine learning #music #art\
●° Henry Faber / #community #VR #games\
●° Jonathan Carroll / <http://toughguymountain.com/> / #VR #interactive\
●° Cat Bluemke / [http://brandscape.club](http://brandscape.club/) / #VR #interactive\
\
| PROJECT LEAD MENTORS\
°● Kyle Duffield + Daniele Hopkins / Electric Perfume\
°● Xavier Snelgrove / [wxs.ca](https://wxs.ca)
°● Izzie Colpitts-Campbell‎ / Dames Making Games Toronto / <https://dmg.to/>\
°● David Psutka / [actactact.bandcamp.com](https://actactact.bandcamp.com/)\
°● Xuan Ye / [a.pureapparat.us/g-a-r-d-e-n](http://pureapparat.us/g-a-r-d-e-n)\
°● Trevor Blumas\
°● Julia Romanowski / [www.juliaromanowski.com](http://www.juliaromanowski.com/)\
°● Monica Bialobrzeski / [monicabialo.com/](http://monicabialo.com/)\
°● Peter Rahul\
°● Projoy Roy\
°● Karl Skene / [www.youngoffenders.co/artists/karl-skene/](http://www.youngoffenders.co/artists/karl-skene/) #generative visuals #lighting design #interactive\
°● Greg Smith / HOLO\
°● Sherry Kennedy / HOLO\
°● Sarah Jane Mortimer\
°● Brian Wong / It's Not U It's Me\
\
| MC\
°● Zoe Daniels / writer/comedian / Hacker U\
\
| Speakers + Workshop Facilitators\
°● Tom Augur / Art+Science\
°● Xavier Snelgrove / https://wxs.ca\
●° Sarah Friend / https://isthisa.com / #blockchain #interactive #generative\
°● Protim Roy\
\
| Adjudication\
°● Jeremy Bailey / Famous New Media Artist\
°● Adam Tindale / OCAD University\
°● Henry Faber / Gamma Space Collaborative Studio\
°● Daniele Hopkins + Kyle Duffield / Electric Perfume\
°● Anthea Foyer / InterAccess\
\
●●○●○○○●●●○○○●○○●●●○○ PARTNERS ○○○\
\
| PRESENTING\
●• Toronto Media Arts Centre\
●• MUTEK Montréal\
| CREATIVE\
•° Gamma Space Collaborative Studio\
•° Dames Making Games Toronto\
•° Charles Street Video\
•° Electric Perfume\
•° The Brandscape\
•° Gen Art Hack Party\
•° HOLO\
•° Canadian Filmmakers Distribution Centre (CFMDC)\
•° It's Not U It's Me\
•° Playnice&Co / [www.letsplaynice.ca](http://www.letsplaynice.ca/)\
\
| SPONSORS\
○● Toronto Laser Services