---
layout: work-item
title: Noye's Fludde (opera)
client: " Trinity St. Paul’s | VIVA! Youth Singers of Toronto | Kingsway Chamber
  Strings"
role: Producer
date: 2023-05-14T23:34:17.660Z
categories: creative
tags:
  - opera
thumbnail: /assets/uploads/noyes_fludde_poster.jpg
---
Produced this unique rendering of Britten’s beloved opera with 100+ local performers: an eclectic mix of Toronto-area youth and amateur musicians alongside professional chamber orchestra and soloists. VIVA! Youth Singers, Kingsway Chamber Strings, Trinity-St. Paul’s, et al.