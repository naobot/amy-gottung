---
layout: work-item
title: Never Grow Up / Ne Grandis Pas
client: KRAK XR, Le Labo
client_link: https://www.krakxr.co/
role: Project and team development; project funding
when: "2021"
where: T﻿oronto, ON
team: Karen Vanderborght, Gillian Bleckenhorst, Maxwell Lander, Omar David
  Rivero, Mehdi Cayenne
date: 2023-05-10T17:54:49.983Z
categories: creative
tags:
  - media
thumbnail: /assets/uploads/screenshot-2023-03-09-at-11.52.20-am.png
images:
  - image: /assets/uploads/screenshot-2023-03-09-at-11.52.37-am.png
---
Do you remember your favourite games you liked to play as a kid?  *Never Grow Up* celebrates traditional children’s games, enciting re-discovery through a series of whimsical immersive puzzles.\
\
But you are not alone. A demanding sidekick cries for your attention and care. This creature will be your companion, antagonist, and nemesis. Time is limited as slime invades the scene while you solve the challenges.\
\
After a stressful first round, shapeshifting wormholes give you a chance to replay the same scenes by changing roles, perspective and strategy.\
\
Game interactions are a metaphor for the mechanics of childhood trauma. They demonstrate both the power (and corruptibility) of positions of control, as well as the dependence of mental health on compassionate relationships.