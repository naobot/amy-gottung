---
layout: work-item
title: Daily Bread
client: Daily Bread
client_link: https://www.dailybread.ca/
role: Consultant
when: 2021-2022
date: 2023-05-28T15:22:31.008Z
categories: consulting
tags:
  - fundraising
  - business_development
thumbnail: /assets/uploads/screenshot-2023-05-28-at-11.23.10-am.png
---
Development strategy and multi-year foundation grant writing for one of Toronto's largest food banks. From its 108,000 sq.ft. distribution hub, Daily Bread supplies food to nearly 200 food programs across Toronto with a fleet of five trucks. Through its research and advocacy, Daily Bread has become a key thought leader locally, provincially and nationally on issues about hunger, life on low income, housing, and income security.