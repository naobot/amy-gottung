---
layout: work-item
title: Artist coaching / consulting
client: "Various (incl): Sarah Ruba, Jessy Lanza / Lanza Trio (documentary film
  project), Golha Iranian Orchestra, Sylvain Sailly (artist) Seyblu (musician),
  Sara Elmagel (filmmaker), Radha Chaddah (artist), Edwin Huizinga (musician),
  Classical / Stereo Live Revolution (independent producer), Andrea Tyniec
  (musician)"
role: Consultant
date: 2023-05-14T20:34:23.440Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/jessy.jpeg
images:
  - image: /assets/uploads/bonnejournee03.gif
  - image: /assets/uploads/sylvain.jpeg
  - image: /assets/uploads/screen-shot-2021-03-13-at-1.24.22-pm.png
  - image: /assets/uploads/seyblue.jpeg
  - image: /assets/uploads/10911496_870904739619956_3039662974693044679_o.jpeg
---
Independent artist consulting: practice and career development, project or grant funding, strategic support