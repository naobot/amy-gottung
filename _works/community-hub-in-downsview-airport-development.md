---
layout: work-item
title: Community Hub in Downsview Airport Development
client: North York Food Bank
client_link: https://northyorkharvest.com/
role: Consultant and grant writer
when: "2024"
team: R﻿yan Noble, Sahar Gafouri and team
date: 2024-10-21T18:18:55.629Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/screenshot-2024-10-21-at-11.27.16-am.png
images:
  - image: /assets/uploads/downsview.jpg
---
S﻿ecured multi-year funding from The Metcalf Foundation t﻿owards the establishment of a community hub a﻿s a central element in the redevelopment of the Downsview Airport lands in North York, Toronto.

North York Harvest Food Bank (NYHFB), along with partners the Learning Enrichment Foundation (LEF) and BuildingUp, is pursuing the establishment of a new community hub as part of the redevelopment of the Downsview Airport Lands.  This collaborative, purpose-built space would combine the community skills development, employment training, and food distribution activities of its partners in the implementation of a holistic approach to poverty reduction and local economic development