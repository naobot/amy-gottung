---
layout: work-item
title: Music in the Barns at the Great Hall
team: Music in the Barns and the Canadian Association for Theatre  Research /
  Association Canadienne pour la Recherche Théâtrale
date: 2023-05-10T20:08:41.658Z
categories: creative
tags:
  - concert
thumbnail: /assets/uploads/18768408_1731966203499253_7550040630860161396_o.jpg
images:
  - image: /assets/uploads/18672928_1721576977871509_4348284877521922164_o.jpg
  - image: /assets/uploads/18891459_1731967236832483_885374064892643030_o.jpg
---
With Artistic Director, produced this extended, inter-media concert. An imaginative transformation of Toronto’s Great Hall, addressing urgent social and environmental issues with interactive installation, live chamber music, and politically-charged performance.

John Cage’s Lecture on the Weather, Michael Oesterle’s Daydream Mechanics, Becoming Sensor installation Room with Ayelen Liberona, filmmaker and dancer, Natasha Myers, anthropologist and dancer, Allison Cameron composer and field recordist, Rosina Kazi & Nicholas Murray of LAL, composers