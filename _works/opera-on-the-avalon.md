---
layout: work-item
title: Opera on the Avalon
link: https://operaontheavalon.com/
date: 2023-05-30T14:22:28.161Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/operashanawdithit-2.jpeg
images:
  - image: /assets/uploads/operashanawdithit.jpeg
---
Grant writing: federal multi-year operational funding for this regional institution, during a pivotal shift in funding models.