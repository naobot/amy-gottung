---
layout: work-item
title: Long Winter Paris
client: Long Winter (Toronto, Can), La Station Gare des Mines (Paris, Fr)
role: Creative Producer / Director
when: September 2019
where: P﻿aris, France
team: Long Winter, La Stations - Gare des Mines, Collectif MU
partners: L﻿e Bureau Export, La Gaîté Lyrique, French Consultate
date: 2023-05-10T17:11:07.918Z
categories: creative
tags:
  - festival
thumbnail: /assets/uploads/76680617_1656593061143575_4368703470631387136_o.jpg
images:
  - image: /assets/uploads/copy-of-doomsquad-photo-guendalina-flamini-16.jpg
  - image: /assets/uploads/75464107_1656594631143418_8357464212884160512_o.jpg
  - image: /assets/uploads/71557321_1059345934456266_9032354891078565888_o.jpg
  - image: /assets/uploads/copy-of-haviah-mighty-photo-guendalina-flamini-31.jpg
  - image: /assets/uploads/70058417_1604892296313652_2192325666370748416_o.jpg
---
With Collectif MU at [La Station](https://lastation.paris/) (Paris, France), co-presented a two-day festival and music industry conference, bringing 40+ Toronto-area artists and industry to Paris: an eclectic roster of under-represented, high-potential talent. Presented an accompanying France-Canada DIY music industry conference at [La Gaîté Lyrique](https://gaite-lyrique.net/en).

\
JESSICA93 • FUCKED UP • NEW FRIES • BUDDY RECORDS • PHÈDRE • ICE CREAM • CARIN KELLY • EDNA KING • JOEL EEL • DOOMSQUAD • HAVIAH MIGHTY • ATELIER CISEAUX • TELEPHONE EXPLOSION • OPALE • MATTHEW PROGRESS • SYLVERE • SCOTT HARDWARE • MYST MILANO. • DJ SUNDAE