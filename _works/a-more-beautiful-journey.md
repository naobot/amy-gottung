---
layout: work-item
title: A More Beautiful Journey
link: https://amorebeautifuljourney.ca/
client: Intersection Festival, Music Gallery, City of Toronto
client_link: "http://www.amorebeautifuljourney.ca "
role: Co-Creator, Creative Producer
when: 2022-2023
where: Toronto, Canada
team: >-
  CREATIVE TEAM


  Amy Gottung (co-creator, creative producer) + Joseph Shabason + Dan Werb (co-creators) 


  Sanjeet Takhar + David Dacks (project design + development - Music Gallery)


  Soundways (geolocated XR audio software)


  Karen Vanderborght (creative technologist)


  Intersection Festival (presenter)


  CURATOR PARTNERS 


  Arts Etobicoke 


  Regent Park School of Music


  Council Fire Native Cultural Centre


  SKETCH 


  RISE 


  Long Winter 


  The Remix Project


  Canadian Music Centre


  REGENT PARK NEIGHBOURHOOD SPONSOR


  The Daniels Corporation


  TECH PARTNERS


  Ableton


  Splice


  PRESENTING AND PROMOTIONAL PARTNERS


  City of Toronto 


  ArtworksxTO


  TTC


  JURY


  Hugh Marsh


  tUkU Mathews


  Cadence Weapon


  Alison Cameron


  Mario Anzola
partners: TTC, ArtworksxTO The Music Gallery, Regent Park School of Music,
  Council Fire, The Daniels Corporation, Taxi Agency, City of Toronto,
  Intersection Festival, Taxi Agency, Soundways, Collectif MU, The Daniels
  Corporation, The Music Gallery, The Remix Project, Regent Park School of
  Music, Council Fire Native Cultural Centre, SKETCH, Arts Etobicoke, RISE, Long
  Winter, Canadian Music Centre, Ableton
date: 2023-04-04 15:25:07 -0400
categories: creative
tags:
  - media
thumbnail: /assets/uploads/screenshot-2023-04-21-at-2.13.05-pm.png
images:
  - image: /assets/uploads/screenshot-2023-06-20-at-1.12.05-pm.png
---
An historic XR (extended reality) audio installation to transform Toronto public transit, in honour of the centennial anniversary of the TTC.

Through a web-based mobile app, riders experience an eclectic mix of site-specific ambient songs, soundscapes, and scores for over 25 discrete stretches of TTC line -- written by musicians from, and for, neighbourhoods spanning the breadth of the public transit grid. These sounds can be experienced through a web-based mobile app.  Musical excerpts unfold generatively for listeners travelling across streetcar or bus routes. AMBJ offers new modes of encounter with our urban geography: simultaneously public and private, intimate and communal, and accessible to millions of daily riders across the TTC.

A﻿RTISTS included:

Absolutely Free 

birthday boy

Brodie West

Casey MQ

Chelsea Stewart

chiquitamagic

Debashis Sinha

Emissive 

Felipe Sena

Jahmal Padmore

Korea Town Acid

Kind Mind

Nick Storring

OBUXUM

Prince Josh

Red Bear Singers

Stefana Fratila

+﻿ more

<iframe width="560" height="315" src="https://www.youtube.com/embed/_mnJavvnby8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>