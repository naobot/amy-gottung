---
layout: work-item
title: Thin Edge New Music Collective
client: ""
role: Consultant
where: T﻿oronto
date: 2023-05-28T15:16:05.626Z
categories: consulting
tags:
  - organizational_development
  - fundraising
thumbnail: /assets/uploads/te70742660_3083814415026352_268537633271971840_o.jpeg
images:
  - image: /assets/uploads/te2.jpeg
  - image: /assets/uploads/te370585264_3083843545023439_2487619068996091904_o.jpeg
---
Helped board and staff develop a marketing and communications strategy for an expanded multi-partner season. Strategic counsel and planning.



Founded in 2011, Tkarón:to (Toronto)'s  **Thin Edge New Music Collective** has emerged as one of Canada's foremost ensembles dedicated to presenting and commissioning composers of our time. Driven by an unquenchable curiosity, they've produced a vast array of innovative and intricate programs, while engaging in unique and ambitious collaborations. In addition to their annual concert season, the “provocative, thrilling, and thought-provoking” (Musicworks) TENMC has been presented on concert stages across Canada and throughout the world.

\
While engaging with internationally-revered composers such as Maria De Alvear, Linda Catlin Smith, Allison Cameron, Mick Barr, Elliott Sharp, James O'Callaghan, Barbara Monk Feldman, Jessie Cox and Ana Sokolovic, the ensemble continues to nurture emerging voices, who comprise a considerable number of their 70+ commissioned works.  It's all part of their distinctly eclectic and community-oriented outlook—endeavouring to bring 20th and 21st century music to an ever-expanding listenership.