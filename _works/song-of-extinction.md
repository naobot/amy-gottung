---
layout: work-item
title: Song of Extinction
client: Luminato Festival, Music in the Barns
role: Producer
when: "2015"
where: Toronto, Canada
team: Rose Bolton, composer / Marc de Guerre, filmmaker / Don McKay, poet.
  Performed by Music in the Barns Chamber Ensemble, Members of Tafelmusik
  Chamber Choir, VIVA! Youth Singers of Toronto, with Music Director John Hess.
date: 2023-05-10T18:07:39.776Z
categories: creative
tags:
  - performance
thumbnail: /assets/uploads/13350264_1349846481711229_393105979275596150_o.jpg
images:
  - image: /assets/uploads/13071693_1324022877626923_3416895025440380892_o.jpg
  - image: /assets/uploads/mitb13507281_10153582815957097_3191387696730185842_n.jpg
  - image: /assets/uploads/13391636_1349846441711233_2941685385101618697_o.jpg
  - image: /assets/uploads/13498038_10154151430973260_3005828345709956989_o.jpg
---
World premiere of live score + film on 25-foot screen with 100+ member ensemble in the Hearn Generating Station at Toronto’s international Luminato arts Festival.\
\
SONG OF EXTINCTION was a visual and sonic experience exploring the calamitous impact humans are having on the planet and its creatures. Featuring stunning large-scale projections and a tightly enmeshed score for live chamber orchestra, chamber choir, electronics, and youth chorus, SONG OF EXTINCTION interwove breathtaking moving images with live surround sound on the Hearn’s towering industrial space.

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/825593866?h=f6ed10f577&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Song of Extinction"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>