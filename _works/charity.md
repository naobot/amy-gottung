---
layout: work-item
title: Charity
link: https://charity.nfb.ca/
client: National Film Board
role: Production Manager
when: "2021"
team: National Film Board of Canada, P﻿FR (Parastoo Anoushahpour, Faraz
  Anoushahpour and Ryan Ferko)
date: 2023-11-11T16:19:20.873Z
categories: creative
tags:
  - documentary
  - media
thumbnail: /assets/uploads/screenshot-2023-11-11-at-10.24.01-am.png
---
P﻿roduction management for 360 video shoots for the National Film Board and MoCCA's co-presentation of this interactive media art project.

North of Toronto, in the centre of a sprawling suburb sits an ornate, unfinished cathedral once surrounded by farmland. Built by a late mining mogul, this structure is now encircled by newly built homes on streets named after members of his family, as well as his prize-winning Holstein cows.

The most famous of these cows was named Charity.

In July 2017, residents of this community awoke to find a chrome replica of Charity, double its original size, suspended on 25-foot tall stilts in the centre of a small crescent facing their homes.

In the cow’s reflection we witness a struggle between a municipal bureaucracy, a wealthy donor and an unsuspecting community, all forced to confront questions of how to represent the identity of a place.