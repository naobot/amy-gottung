---
layout: work-item
title: The Laneway Project
client_link: https://www.thelanewayproject.ca/
date: 2023-05-14T19:59:21.904Z
categories: consulting
tags:
  - fundraising
  - organizational_development
thumbnail: /assets/uploads/tlpscreen-shot-2022-01-02-at-6.17.05-pm.png
---
Organizational development consulting, grant writing, during a critical transition period for this space-based non profit.

T﻿he Laneway Project:

\
We know that laneways have the potential to be vibrant, living spaces - and that thriving public spaces help to create strong neighbourhoods, communities and cities.

1. We work in partnership with the development and design community to upgrade and activate laneways.
2. We work with municipalities and other stakeholders to create laneway-friendly policies and procedures.
3. We develop resources and host events to inspire and support residents, community groups, businesses and other stakeholders in improving and making better use of their local laneways.