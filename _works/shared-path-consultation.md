---
layout: work-item
title: Shared Path Consultation
link: https://sharedpath.ca/
client: "Shared Path "
client_link: https://sharedpath.ca/
role: Consultant
when: 2﻿018
where: ""
team: ""
date: 2023-11-05T20:02:05.409Z
categories: consulting
tags:
  - Indigenous
  - development
thumbnail: /assets/uploads/sun.jpeg
images:
  - image: /assets/uploads/sharedpathcab11d6f805403e00b6d57067ce5bfba.jpeg
---
D﻿evelopment consulting for this land planning consultancy focused on Indigenous rights and engagement.

Shared Path Consultation Initiative (Shared Path) is a charitable organization that is addressing the challenges and opportunities that emerge where land use change and Aboriginal and Treaty Rights intersect. Planning, as a political and sometimes contentious process, has the potential to impact Indigenous political and territorial claims. We seek to provide opportunities and resources that enhance, inform, and facilitate Indigenous-non-Indigenous bridge building, particularly within the realm of land planning practice.

In response to the TRC Calls to Action 47, 57, and 92, Shared Path provides resources for First Nation, Métis, municipal, and provincial government, as well as professionals in land use change to better engage in consultation and relationship-building. Shared Path connects and supports a network of people urging legislative reform to provide clarity in these processes. Shared Path develops opportunities and resources to advance a broader public understanding of the historical, political, cultural, and environmental factors that inform planning. Shared Path creates opportunities for education and information sharing through workshops, seminars, and projects designed to facilitate relationship-building.