---
layout: work-item
title: Together Apart
link: https://toronto.paris/
role: Creative Producer / Director
when: September - November 2021
where: T﻿oronto, Canada; Paris, France
partners: "[Collectif MU](https://lastation.paris/), [Collectif
  Ascidiacea](https://ascidiacea.org/), [University of Ryerson (Faculty of
  Communication and Design)](https://www.ryerson.ca/fcad/), City of Paris, City
  of Toronto, French Consulate, Université de Rouens Music Ontario, Club
  Quarantine, Unit 2, Sou Sou, Strangewaves, FSR Radio, Gentrification Tax
  Action, Bunker 2, True Connection, ISO Radio, Not Dead Yet, The Shell
  Projects, Debaser, On Earth, Gendai, Making with Place, Xpace, Lula Lounge,
  Together Apart Research Conference, Music Ontario, InterAccess, Canadian
  Independent Music Association, Dundas West-Little Portugal BIA, St. Anne’s
  Anglican Church, The Garrison, Process, Studio AM, Réseau Map,  Manifesto XXI"
date: 2023-04-19T18:07:06.818Z
categories: creative
tags:
  - festival
  - conference
thumbnail: /assets/uploads/6r0a6842.jpg
images:
  - image: /assets/uploads/shane-parent-crowd01.jpg
  - image: /assets/uploads/6r0a5562.jpg
  - image: /assets/uploads/shane-parent-house-of-siriano.jpg
  - image: /assets/uploads/shane-parent-dj-mirass.jpg
  - image: /assets/uploads/shane-parent-asuquomo.jpg
  - image: /assets/uploads/shane-parent-cerena.jpg
  - image: /assets/uploads/shane-parent-karim-olen-ash.jpg
  - image: /assets/uploads/shane-parent-dj-mirass.jpg
description: International DIY music and art Festival, co-presented by Long
  Winter (Toronto) and La Station (France).
---
International DIY music and art festival , showcase, and conference  co-presented by Long Winter and La Station. 

Sept 24 + 25 // Nov 26 + 27 

Toronto, Ontario // Paris, France // Virtual 

Fieldgate Lot // St. Anne’s Anglican Church // Lula Lounge // Xpace // InterAccess Gallery

Second instalment of a two-part, binational exchange: multi-arts festivals, conference, editorial, and industry events, co-produced and co-programmed by Collectif MU/La Station (Fr) and Long Winter (ON): September 20-21, 2019 (at La Station in Paris, France), and Sept 24-25 (Dundas West - Fieldgate Lot / The Garrison) and Nov 26-27 (St. Annes, Lula Lounge)

* An international conference on DIY scenes and spaces: Nov 27, based at Lula Lounge, St. Anne’s (651 Dufferin), and streamed internationally
* 2﻿ nights of live shows and art installations on Dundas West (Fieldgate lot); all performances filmed for online showcase.
* 13 international, cross-sector panels and talks on issues of equity, DIY space, accessibility, networking, and more (Building and Sustaining DIY spaces, DIY outside the core, DIY across Ontario, Hip Hop & DIY, Building Trust & Advocacy for the DIY sector + much more)
* Over 9 free IRL workshops for local DIY organizers, artists, industry, and community members that included social mentorship lunches, technical workshops with free tools (Serato workshop with Freeza Chin, Ableton workshop for trans and female-identifying participants by Ciel, Touchdesigner integration of visuals x music workshop with Karl Skene), and practical, community-building workshops (co-visioning future spaces with It's OK*, Budgeting for your DIY event with On Earth, Funding your project with Josephine Cruz, ISO Radio, et al.)
* B2Bs for labels, promoters, tour bookers, and music agents -- included local workshops as well as panels and keynotes 
* Offered in parallel to an academic symposium (not included in our activity / budget), co-presented with Ryerson Faculty of Communication and Design, ADAIR, that focused around space precarity for DIY scenes in urban environments
* 4-day international music festival and industry showcase, presented through digitally broadcast and live performances from a range of southern Ontario music scenes, alongside visual art, installation, media art, dance, more 

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/825610008?h=c1a0feecde" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>