---
layout: work-item
title: New Music Concerts
link: https://www.newmusicconcerts.com/
role: Consultant
date: 2023-05-14T20:02:50.007Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/nmc87405183_2859399697416765_9202028274186715136_o.jpeg
images:
  - image: /assets/uploads/nmc101395948_3080432185313514_816686566750552064_o.jpeg
  - image: /assets/uploads/nmc141694612_3742902815733111_8934983121428120442_o.jpeg
---
F﻿undraising and development support: operating grants