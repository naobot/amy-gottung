---
layout: work-item
title: Paradigm Productions
role: Consultant
date: 2023-05-28T15:20:26.198Z
categories: consulting
tags:
  - project_development
thumbnail: /assets/uploads/paraimg_1583.jpeg
images:
  - image: /assets/uploads/paraimg_9578.jpeg
---
Consultation and coaching for this independent new theatre company: production budgeting and planning for an epic trilogy supported by a Canada Council New Chapter Grant.