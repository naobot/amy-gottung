---
layout: work-item
title: It’s Not U It’s Me
link: http://itsnotuits.me/
client: It’s Not U It’s Me
client_link: http://itsnotuits.me/
role: ""
date: 2023-04-04 15:25:07 -0400
categories: consulting
tags:
  - organizational_development
thumbnail: /assets/uploads/13239405_1760033877542971_1046369470115057772_n.jpg
images:
  - image: /assets/uploads/inu13267836_1760033974209628_5867747853166996939_n.jpeg
  - image: /assets/uploads/inuim13680422_1778816365664722_6816700129201608265_o.jpeg
  - image: /assets/uploads/inu13669405_1778817378997954_8953432406657184711_o-1.jpeg
  - image: /assets/uploads/inu13640763_1778816205664738_6421660223644937972_o.jpeg
---

 Strategic counsel and support for DIY music promoter and ad-hoc network of local dance scenes, electronic artists and collectives.