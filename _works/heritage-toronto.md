---
layout: work-item
title: Heritage Toronto
date: 2023-05-14T20:24:22.592Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/downloadherto.jpeg
---
Fundraising and communications support: annual gala