---
layout: work-item
title: Alter-Places
link: https://alterplaces.com/
client: Creative Europe, Sorbonne Nouvelle, La Station-Gare des Mines
role: Core partner, curator, researcher
when: 2023-2024
where: E﻿urope
team: N﻿atalia Bobadilla, Olivier LeGal, Gaspard Bourgeois, et al.
partners: E﻿uropean Union
date: 2024-10-31T17:05:48.283Z
categories: consulting
tags:
  - cultural_space
  - research
  - programming
thumbnail: /assets/uploads/screenshot-2024-10-31-at-10.35.53-am.png
images:
  - image: /assets/uploads/img_7394.jpg
  - image: /assets/uploads/whatsapp-image-2024-09-07-à-10.21.35_cecb52bf.jpg
  - image: /assets/uploads/img_1494.jpg
  - image: /assets/uploads/whatsapp-image-2024-09-05-à-19.46.20_32008ca7.jpg
---
A﻿ collaborative initiative by a network of cultural spaces, researchers, and networks across Europe and Canada, and funded by a grant from the European Union, Alter-Places is a cooperative project about sustainable practices, exploring the value and contribution of sustainable practices implemented by Alternative Cultural Places in the development of green, fair, diverse urban ecosystems through collaborative work. 

The project extends over 2024-2025 and brings together 5 European alternative cultural places, the Canadian expert DIY Space Project (a project I founded in 2021), the European network Trans Europe Halles and the interdisciplinary research laboratory LabEx ICCA. 

Over the last 20 years, Alternative Cultural Places (ACPs) have emerged across Europe through the actions of creative workers. The covid-19 crisis highlighted the role of ACPs in our local ecosystems as laboratories of sustainable practices. However, the unique value of these places remains unclear.

ALTER-PLACES explores the value and contribution of sustainable practices implemented by ACPs in the development of green, fair, diverse urban ecosystems. Taking as starting point a holistic view of sustainability (social, economic and environmental) this cooperative project has 4 objectives: 1) Explore the innovative sustainable practices developed by ACPs and evaluate their role in urban resilience 2) Identify & assess obstacles and tensions that ACPs face in developing sustainable strategies 3) Build the capacity of ACPs to implement and monitor durable projects through exchange of best practices 4) Raise public authorities’ awareness regarding the role and value of ACPs in fair urban renewal.

ALTER-PLACES has been conceived with a pluri-disciplinary, participatory and mixed methods approach. The project design is composed of 4 phases: project set up, surveying & mapping, co-constructing, prototyping & testing, disseminating & evaluating. Our target groups include creative workers in ACPs, ACPs’ stakeholders, and local, national and European networks. These groups will benefit from a mapping of innovative practices developed by ACPs across Europe, 4 prototypes to evaluate sustainability approaches among ACPs, to implement relevant methods, to mutualize resources and to improve audiences’ experience. Finally, the project will generate critical data about the state of sustainability considerations within and among ACPs and communities, and explore implications of different political contexts (Ukrainian, European and Canada). Our intent is to trigger and enhance the capacity of ACPs as key drivers of sustain-ABLE practices in urban ecosystems and the cultural sector.