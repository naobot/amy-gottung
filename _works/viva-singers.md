---
layout: work-item
title: VIVA Singers
role: Consultant
date: 2023-05-30T18:58:35.668Z
categories: consulting
tags:
  - organizational_development
  - fundraising
thumbnail: /assets/uploads/viva1606319_10152990712443260_5719237822731196107_o.jpeg
images:
  - image: /assets/uploads/viva10624061_10152990724273260_1737137128764355457_o.jpeg
---
O﻿rganizational development and grant consulting to this downtown choral program for children, adults, and youth, with and without disabilities.