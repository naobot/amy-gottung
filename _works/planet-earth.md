---
layout: work-item
title: Planet Earth
link: https://www.cinesymphonyplanetearth.com/
client: Dynamic Maestro
role: Archival film research
team: Dyan Machan, Johan de Meij, Jed Parker
date: 2023-07-12T16:15:35.312Z
categories: creative
tags:
  - film
thumbnail: /assets/uploads/screenshot-2023-07-12-at-6.19.59-pm.png
---
Archival stills and footage research for this 50-minute visual accompaniment to a new symphony by Johan de Meij. For Director-Editor Jed Parker.