---
layout: work-item
title: Speaking engagements
client: Various
date: 2023-05-14T21:07:40.175Z
categories: speaking
tags:
  - speaking
thumbnail: /assets/uploads/amyspking.jpeg
images:
  - image: /assets/uploads/dsc08544.jpg
  - image: /assets/uploads/120114149_2876588355776008_7925218672823700763_o.jpg
  - image: /assets/uploads/capitalism-updated.png
  - image: /assets/uploads/dsc08570.jpg
---
[Alter-Places](https://alterplaces.com/) workshop - Malmö, Sweden - [Hypnos Theatre](https://hypnostheatre.com/)[](https://www.teh.net/events/teh-conference-97-in-tartu/)[](https://www.mtl2424.ca/en/)\
M﻿oderator

[Arts of Survival](https://www.teh.net/events/teh-conference-97-in-tartu/) - Trans Europe Halles (2024) [](https://www.mtl2424.ca/en/)\
M﻿oderator: Alternative cultural spaces under crisis: resilience, renewal, and challenges\
C﻿o-Programmer with Izolyatsia (Ukraine), La Station-Gare des Mines (France), Lab-Ex (France) - panel series

Sommet de la Nuit - [Mtl2424](https://www.mtl2424.ca/en/)(2023)\
Panelist: DIY Spaces

[Space of Urgency](https://spaceofurgency.org/) Conference -* [Frei(t)räume](https://spaceofurgency.org/project/freitraeume/)* (2022)\
Co-programmer and speaker: Building a Global Cultural Mycellium\
\
*Towards an Alternative Vision of Creative and Cultural Third-Places* (2022)\
[Université de Sourbonne Nouvelle](http://www.univ-paris3.fr/) et al. \
Panelist: Alternatives and Policies\
\
[ArtLab -](http://artlab.paris/) La Station Gare des Mines, Paris (2022)\
Presenter: A More Beautiful Journey x Soundways AR collaboration\
\
[Canadian Music Centre](https://cmccanada.org/) AR composition workshop (2022)\
Panelist \
\
[Indie Week Canada](https://www.indieweek.com/)(2022)\
Panelist: Industry Insights\
\
[Trans Europe Halles Conference](https://teh.net/event/teh-conference-93-in-prague/) (2022)\
Presenter: Toronto DIY Space Project\
\
City of Toronto Music Office\
Masterclass: DIY presentation and promotion\
\
[Venus Fest](https://www.venusfest.net/)\
Panelist: “Relocalizing our music scene” \
\
[Global TO](https://www.globaltoronto.org/)\
Moderator: capitalism panel with panelists Rosina Kazi (LAL, Unit 2), ShoShona Kish (Digging Roots), and musician Ian Kamau.\
\
[University of Toronto - entrepreneurship](https://entrepreneurs.utoronto.ca/)\
Panelist: multiple workshops presented by ONRamp, the University of Toronto’s Entrepreneurship Centre and the university’s [Faculty of Music](https://music.utoronto.ca/)\
\
[Seneca](https://www.senecacollege.ca/home.html) College\
Guest lecturer: PR and publicity in the non-profit sector\
\
[Artspond: This Space - Lessons from the Landscape ](https://artspond.com/2020/09/24/this-space-lessons-from-the-landscape/)\
Round table on spatial precarity, gentrification, and the arts\
\
[Third Spaces conference - École nationale supérieure d'architecture de Paris-La Villette (ENSA Paris-La Villette)](https://adair3p.hypotheses.org/133)\
Speaker and collaborative think tank participant at international cross-sector research conference on creative and cultural third spaces, run by ADAIR (Art and Design Actions for Inclusive Renewal)\
 \
[University of Toronto](https://www.utoronto.ca/)\
Panelist and speaker for University of Toronto’s Lives in Music and Music and Entrepreneurship courses