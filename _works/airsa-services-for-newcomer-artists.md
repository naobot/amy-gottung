---
layout: work-item
title: Airsa Services for Newcomer Artists
role: Consultant
when: "2017"
date: 2023-05-28T15:14:49.658Z
categories: consulting
tags:
  - organizational_development
  - research
thumbnail: /assets/uploads/airsa1-.png
---
Funded by an OAC Compass Grant, guided non-profit through first pivotal period of organizational development, delivering a comprehensive SWOT including an external scan on the newcomer artist service space in the GTA, and over 15 interviews with staff, participants, board members, and others in the sector. Provided high-quality documentary video recordings for research and communication purposes. Offered organizational audit and strategic analysis for staff and board, in preparation for strategic planning.