---
layout: work-item
title: Phoenix Chamber Choir
role: Consultant
date: 2023-05-30T14:25:22.153Z
categories: consulting
tags:
  - organizational_development
  - fundraising
thumbnail: /assets/uploads/phoenscreen-shot-2021-03-22-at-8.48.33-am.png
images: []
---
Provided strategic counsel and grant support during critical period of leadership transition for this high-performing 30-voice chamber ensemble.