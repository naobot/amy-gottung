---
layout: work-item
title: Level Justice
link: https://www.leveljustice.org/
role: Consultant
when: 2018-2020
date: 2023-05-30T14:17:21.373Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/ljnya_2018-29.jpeg
---
Organizational development consulting for this national social justice education and professional training non-profit, empowering communities to understand, shape, and use the law then they can become active participants in breaking down barriers to justice.

Level is a Canadian justice education charity.bWe level barriers to justice by building empathy, increasing equity, and advancing social justice. We deliver programs nationwide, in support of a justice system that treats everyone with dignity and an informed society where everyone can exercise their rights and has the opportunity to thrive.

We deliver programs - to youth, law students, and lawyers - focused on innovative justice education, social justice awareness, legal mentorship, and access to justice training.