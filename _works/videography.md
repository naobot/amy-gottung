---
layout: work-item
title: Videography
client: The Kit Magazine, Sweet Potato Chronicles, Glenn Gould Festival,
  Tapestry Opera, Dan Lim Photography, Plum TV
role: Director/shooter/editor
when: 2006 - 2014
where: ""
date: 2023-05-10T18:04:13.071Z
categories: creative
tags:
  - media
  - video
  - documentary
  - television
thumbnail: /assets/uploads/ddscreen-shot-2021-03-09-at-9.16.16-am.png
---
Producer/camera/editor:vnews, documentary, lifestyle, and behind-the-scenes video for digital, broadcast, editorial and commercial clients.  

Clients include: **Plum TV (Preditor - 2008),﻿ The Kit Magazine (2010),** **S﻿weet Potato Chronicles (2011), Dan Lim Photography (2011), T﻿apestry Opera (2016), Glenn Gould Symposium (2014)**