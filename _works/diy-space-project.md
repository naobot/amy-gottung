---
layout: work-item
title: DIY Space Project
link: https://torontolongwinter.com/diyspace
client: Trans Europe Halles et al.
role: Founder / Director
when: 2022-2023
partners: Trans Europe Halles, City of Toronto, The Toronto Metropolitan School,
  Vibe Lab, et al.
date: 2023-04-19T20:16:53.980Z
categories: consulting
tags:
  - sector_development
  - research
thumbnail: /assets/uploads/screen-shot-2022-02-12-at-2.48.18-pm.png
images:
  - image: /assets/uploads/photo-2022-06-03-15-23-44.jpg
  - image: /assets/uploads/photo-2022-06-03-15-23-44-1-.jpg
  - image: /assets/uploads/dspphoto-2022-06-03-12-23-43.jpeg
  - image: /assets/uploads/photo-2022-06-03-09-03-29.jpg
  - image: /assets/uploads/photo-2022-06-02-02-53-12.jpg
  - image: /assets/uploads/photo-2022-06-02-02-53-11.jpg
  - image: /assets/uploads/dspimg_1618.jpeg
  - image: /assets/uploads/dspimg_1397.jpeg
  - image: /assets/uploads/photo-2022-06-01-12-37-32.jpg
  - image: /assets/uploads/img_1335.jpg
---
TORONTO DIY SPACE PROJECT - INTERVENTIONS FOR ALTERNATIVE CULTURAL SPACES

Peer-driven, locally focused, internationally informed, the Space Project is a direct intervention in support of the preservation and stimulation of self-organized, alternative culture. We are working for urgent results and long-term shifts.

**THE PROCESS**

From a city-wide open call and juried process, three collectives seeking access to space were selected for participation.

Collectives are paired with a neighbourhood-focused cross-sector advisory group to support them in their journey to find, secure, or sustain new spaces. Advisory groups are curated in coordination with the applicant, based on individual context. Members are drawn from spheres ranging from research, urban planning, real estate, building ownership and operation, and government.

The experience is documented to inform policy and opportunities for alternative cultural and social spaces, in Toronto and elsewhere.

Participation includes:

* compensation
* construction of a work plan focused on immediate needs (e.g. finding or securing space, building a governance or business model, pursuing partnerships, forming networks, fundraising, conducting advocacy)
* facilitated group meetings and one-on-one coaching with advisors
* local connections and introductions to open longer-term opportunities
* research to support policy recommendations

**INTERNATIONAL EXCHANGE**

The DIY Space Project is an associate member of project partner [Trans Europe Halles](https://teh.net/), an international network of grassroots cultural spaces across Europe and beyond.

Thanks to the generous support of Trans Europe Halles, in May of 2022 representatives from each of our collectives attended the network’s annual conference, and participated in a multi-venue, multi-city tour in Prague, Amsterdam, and Berlin. We met and exchanged with community-run spaces, advocacy networks, and government officials engaged in grassroots space policy. We experienced new models, built connections, and compared environments and challenges.

In each city, government officials from Toronto and the host municipality exchanged contexts, advancements, and best practices.

Instructive cases are informing project paths for each group. In coordination with local government, we are exploring integrations and adaptations back at home.

**GOALS**

…proceed in tandem across multiple timeframes and systemic levels: targeted outcomes for participants, and changes to facilitate the cultivation of more space for alternative, experimental, non-commercial activity in twenty-first century cities.

* **Community** - **results for participating collectives**

  The cohort receives expert input, strategic relationships, and inter-sector knowledge and training around finding and establishing more sustainable space for their communities, and others in their network.
* **Municipal** - **program and policy advocacy in Toronto**

  Local and international case studies provide valuable data for both cultural groups and policy makers. Triangulated conversations between sector leaders (building ownership, municipal and provincial government, urban planning, grassroots cultural/social work) help inform best practices for cross-sector exchange. New connections - between collectives and across sectors - open advocacy opportunities.

  With the facilitation support of [PROCESS](https://weareprocess.ca/), we are brokering unconventional dialogues and relationships to surface shared interests and collaborative solutions.

  In parallel, we are coordinating with members of local government (across sectors and regional levels) to identify and shift present hurdles to community space use.
* **International** - **multi-city research and exchange**

  Rapidly developing “world” cities are in urgent need of solutions-focused exchange around space scarcity and affordability. We aim to build awareness and critical dialogue around models in support of counter-cultural and community-driven spaces. Categories of investigation include (but are not limited to) internal governance and economies, external platforms and networks, advocacy practices, business and funding models, sustainability, gentrification implications, inter-sector partnerships, and government policies.

**PARTICIPATING COLLECTIVES**

**[Hearth](http://hearthgarage.com/)**

Founded in 2019 as an artist-run space and DIY gallery, Hearth seeks to provide a site for projects within a context that values collaboration, experimentation, and community. Hearth is committed to working towards an anti-oppressive, queer positive environment, and welcoming marginalized, racialized, and indigenous folks through programming that celebrates the work of a diverse range of emerging collaborators. As a collective, we are able to respond quickly to artists’ needs, and aim to provide a space where artists have the chance to pursue projects that benefit from a flexible setting. Hearth is composed of four members, Sameen Mahboubi, Philip Leonard Ocampo, Rowan Lynch, and Benjamin de Boer. The name makes reference to our goals. As a structural element in the makeup of a house, and a tool providing warmth, light, and food; a hearth gathers us towards itself, and towards each other. Hearth programming presents exhibitions, public programming including performances, readings, screenings, workshops, tours and digital projects through our curatorial mandate.

**[RISE](https://www.torontolongwinter.com/www.riseedutainment.com)**

Reaching Intelligent Souls Everywhere (RISE) is a youth-led movement that began in Scarborough in 2012, providing opportunities and spaces for youth to develop artistically, professionally, personally, socially, and spiritually. RISE creates safe and inclusive spaces that foster self-expression and healing through the performance arts and storytelling. R.I.S.E’s mission is to create: (1) opportunities for young artists to participate in and deliver workshops that empower youth to develop confidence in themselves and their voices through self-expression in a supportive atmosphere; (2) opportunities for youth to showcase their work across the GTA in schools and community settings; (3) platforms for new, emerging, and professional artists to build careers in the arts, and; (4) Employment and volunteer opportunities for BIPOC young people to participate in community-service. RISE’s programs provide youth with opportunities to express their creative voice, develop artistic and innovative leadership skills, and build capacity for professional artistic practice, while encouraging them to imagine alternative futures for themselves and their community. RISE is dedicated to being that shelter of positivity for young people experiencing financial precarity and at risk of violence in their community. It turned out to be something that many young people were hungry for; an opportunity to share, feel safe, express themselves, and practice their art. RISE has since grown into the registered not-for-profit arts and community service that it is today. In 2019, prior to the pandemic RISE organized 374 events, hosted 3310 performing artists on our stage, and gathered over 25,000 recorded attendees.

**[Our Women’s Voices](https://www.instagram.com/ourwomensvoices/?hl=en)**

“We are a platform dedicated to amplifying the voices of women and making social change through the arts, community and education. We curate events that provide womxn, femme & NB folks an outlet to use their voices, expand their artistry, learn from other womxn and give youth activism opportunities to reshape the future. Our vision is to have a space to run workshops and events focused on many themes such as violence against women, creative careers, feminism and reclamation.”

**HOW APPLICATIONS WERE EVALUATED**

Applications were evaluated by an external jury — members listed below.

Participating collectives were identified based on degree and variety of need, potential for the participating community, and diversity of representation and locations, among selected collectives. Equity-seeking communities were prioritized.

- - -

**CROSS SECTOR ADVISORS**

Tura Cousins Wilson, SOCA

Kate Murray, TAS Impact

Ariana Holt, TAS Impact

Tiffany Fukuma, Trans Europe Halles

Phil Brennen, Brocolini

Kendra Fry, Creative Collisions

Michael McLelland, ERA Architects

Melissa Daly-Buajitti, CBRE

Oliver Pauk, Akin

Chris Wilson, It’s OK*

Rupal Shah

Shans Ashley, Manifesto

Noa Bronstein, TPW

Thomas Scheele, Vibelab

Kofi Hope and Zahra Ebrahim, Monumental Projects

Sheena Jardine-Olade, Night Lab

Mike Tanner - City of Toronto, Music Office

Ben MacIntosh, Erika Hennebury, Mojan Jianfar - City of Toronto, Office for Creative Space

**STEERING COMMITTEE / PARTNERS**

Just some of our generous early advisors and supportive partners include:

**[Trans Europe Halles](https://teh.net/) -** Tiffany Fukuma (Managing Director), Ella Overkleeft

**[ADAIR](http://adair-thirdplaces.com/index.php/journal/)** / UNIVERSITÉ DE ROUEN **\-** [Natalia Bobadilla,](http://adair-thirdplaces.com/index.php/team/) Researcher

**[ERA Architects](https://www.eraarch.ca/)**

**Vibe Lab**

**[CP Planning](https://cpplanning.ca/)** **\-** Cheryll Case, Founder

**City of Toronto -** Pat Tobin, Mike Tanner, Sally Han (York University), Christina Heydorne, Alok Sharma, Erika Hennebury, Ben MacIntosh, Marguerite Pigott

**[PROCESS](https://weareprocess.ca/) -** Sara Udow and Nadia Galati

**JURORS**

**Petrina Ng (she/her)** - Petrina Ng is an artist and organizer based in Tkaronto/Toronto. Her practice proposes alternative responses to redress subtle legacies of colonialism. Petrina’s collaborative work as Gendai (with curator Marsya Maharani) responds to BIPOC labour conditions of arts work. Their research and practice of collective values experiments with alternative economies and radical allyship to work towards a more equitable arts sector. She is also co-founder of Durable Good, a small publishing studio that supports artists, writers, and thinkers who work within feminist, equitable, and engaged frameworks; and newly launched Waard Ward collective that utilizes floristry as a means to embody decolonial research and newcomer engagement.\
[www.petrinang.com](http://www.petrinang.com/), www.gendai.club

**Max ZB (He/They)** - Maxhole is a musician, photographer, performer and curator.\
He programs and curates for community art space Unit 2, as well as co-organizes Queer arts festival Bricks & Glitter. His art and performances are streams of consciousness and reflections on his life thus far. Maxhole actively performs around Toronto (NXNE, The AGO, TedTalk) and works with the arts community to uplift marginalized people and influence an alternative generation of creatives. Focus on uplifting Queer and BIPOC folks ALWAYS.\
<https://www.instagram.com/maxholio/>

**rosina kazi** - any pronouns - Rosina Kazi is the lead singer of the protest electronic duo LAL, who were long listed for the Polaris Prize for 2019 and 2021. They/she are a queer/gender fluid, culturally Muslim and Bengali identified artist. Rosina helps run the alternative DIT (Do it Together) community and arts accessible (physically and financially) space Unit 2, a space dedicated to supporting QT2SBIPOC and friends in order to support an arts ecosystem verses industry. Rosina also co-curates shows, participates in theatre and other collaborative art making practices and runs workshops around sound, recording and poetry.

Rosina currently lives and was born in the territory of the Mississaugas of the Credit, the Anishnaabeg, Haudenosaunee, Lenni-lenape and the Wendat Nations.\
[www.lalforest.com](http://www.lalforest.com/) / [http://unit2.club](http://unit2.club/)

**Melissa Daly-Buajitti** (she/her) - Melissa is a commercial real estate professional with seven years of experience across the fields of brokerage, architecture and construction - most recently at CBRE. She is a dedicated member with the Urban Land Institute (ULI) Toronto, where she led an initiative harnessing the expertise of ULI’s network to help a grassroots arts collective develop their real estate strategy.

Melissa holds a Master of Design in Strategic Foresight and Innovation from OCAD University and a Bachelor of Humanities from Carleton University. She is studying for a Master of Real Estate and Infrastructure at York University’s Schulich School of Business.

**R. Flex** is an electro-R&B artist who has been named one of Now Magazine's Artists to Watch in 2020 & 2021. Their live performances have helped build a name for them in Canada's music scene. Their debut EP “In & Out” received over 30K streams and their song “Thursday” was a Top 20 finalist at Imsta Festa Toronto. They have been featured on Paper Magazine, YOHOMO, & CBC Here & Now and are set to drop their next EP “Flex With Benefits” in 2022. 

**PROJECT LEAD**

**Amy Gottung** became a de-facto space advocate through years of work within and between Toronto’s DIY communities. She is a former collective member and executive director of Long Winter. With Long Winter and in partnership with [La Station-Gare des Mines](https://lastation.paris/) in Paris, France, she initiated and led a multi-year, space-focused exchange between DIY scenes in Paris and Toronto that included a research conference, local workshops, inter-sector dialogues, and editorial projects, alongside live music and art.

\
This project is funded in part by a Canada Council Sector Innovation and Development grant.