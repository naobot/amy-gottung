---
layout: work-item
title: North York Harvest Food Bank
link: https://northyorkharvest.com/
role: Consultant
when: 2017-2020
where: T﻿oronto
date: 2023-05-14T20:31:20.459Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/ny101204793_10157951611707489_2040382762958979072_o.jpeg
images:
  - image: /assets/uploads/ny102908927_10157980775697489_2594631342749068795_o.jpeg
  - image: /assets/uploads/ny118248409_10158217428522489_8971954647807769768_o.jpeg
---
Grant writing and strategic development for North York’s largest food bank, serving 16,000 people each month through over 60 community programs.\
\
Projects include:

* Metcalf Inclusive Economies
* Trillium
* Fidelity Foundation Social Investment funds 
* Foundation grants