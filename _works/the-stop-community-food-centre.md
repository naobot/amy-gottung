---
layout: work-item
title: The Stop Community Food Centre
client: The Stop Community Food Centre
client_link: http://thestop.org/
when: 2017-2024
date: 2023-04-19T20:14:38.030Z
categories: consulting
tags:
  - fundraising
  - evaluation
thumbnail: /assets/uploads/screenshot-2023-05-30-at-8.52.20-am.png
images:
  - image: /assets/uploads/screenshot-2023-05-30-at-8.52.46-am.png
  - image: /assets/uploads/screenshot-2023-05-30-at-9.27.19-am.png
  - image: /assets/uploads/screenshot-2023-05-30-at-8.53.42-am.png
  - image: /assets/uploads/screenshot-2023-05-30-at-8.45.19-am.png
---
Fundraising and development strategy, grant and proposal writing, program evaluation design and communications support for this progressive food security, urban agriculture, and community advocacy organization in Toronto’s west end.