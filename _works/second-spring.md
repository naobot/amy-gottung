---
layout: work-item
title: Second Spring
link: https://secondspring.online/
client: Second Spring
client_link: https://secondspring.online/
role: "Collective "
team: J﻿ulian Hou, Matt Smith, Fan Wu, SF Ho, Kasper Feyrer, Eddy Wang et al.
date: 2023-07-11T10:16:41.476Z
categories: creative
tags:
  - art
  - music
  - fundraising
thumbnail: /assets/uploads/touch-the-ten-thousand-things-without-dependency_front.jpg
---
Ensemble, programming, project development