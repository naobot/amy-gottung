---
layout: work-item
title: Riot Club
link: ""
client_link: https://riot.club/
role: Consultant
when: ""
where: L﻿A / Toronto
date: 2023-05-28T15:01:32.855Z
categories: consulting
tags:
  - business_development
  - organizational_development
  - fundraising
thumbnail: /assets/uploads/screenshot-2023-05-28-at-11.07.18-am.png
images:
  - image: /assets/uploads/screenshot-2023-05-28-at-11.06.40-am.png
  - image: /assets/uploads/screenshot-2023-05-28-at-11.06.10-am.png
---
B﻿usiness development and funding consulting for this fast-rising creative industries development network, building mentorship, connections, and engagement between Los Angeles and Toronto.