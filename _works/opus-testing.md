---
layout: work-item
title: "Opus: Testing"
client: Canadian Music Centre, Musica Reflecta, et al.
date: 2023-05-10T20:15:55.918Z
categories: creative
tags:
  - concert
thumbnail: /assets/uploads/img_2274.jpg
images:
  - image: /assets/uploads/img_2272.jpg
  - image: /assets/uploads/12004891_968011019927598_7432093941713012849_n-1-.jpg
  - image: /assets/uploads/img_2233.jpg
---
Co-programmed and produced series of community-driven composer/ensemble workshop series at Canadian Music Centre. Facilitated collaborations between composers and   filmmakers, DJs, dancers, scientists, carillon towers, period ensembles, live electronics, traditional Chinese instruments, NASA field recordings (image and sound), and more. Co-presented interdisciplinary one-day collaborative intensive with Canadian Music Centre and Jumblies Theatre.