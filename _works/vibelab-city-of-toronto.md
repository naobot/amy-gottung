---
layout: work-item
title: Vibelab / City of Toronto
client: Vibelab
role: Lead local consultant / Facilitator
date: 2023-05-14T21:11:37.217Z
categories: consulting
tags:
  - sector_development
  - research
thumbnail: /assets/uploads/nighteconomyreview-1920x1080-1.jpg
---
Lead local consultant and partner to Vibelab, a global leader in purpose driven consultancy for nighttime culture. We developed and conducted a local outreach and engagement process, in order to understand the industry and community’s most pressing needs. 

As Toronto’s nightlife and cultural scenes recover from the harshest impacts of Covid-19, the City of Toronto undertook initiatives to review the zoning, licensing, and planning policies that influence hospitality, entertainment, and nightlife in the city.