---
layout: work-item
title: Should I Be Scared of This?
role: Co-Producer
when: "2017"
where: T﻿oronto, Canada
team: Jordan Foisy, Chris DePaul, Miles DePaul
date: 2023-05-10T17:30:22.699Z
categories: creative
tags:
  - media
thumbnail: /assets/uploads/sibsot-image.jpg
images: []
---
Comedian Jordan Foisy lives in a scary place. We all do. It’s called The World. From rising sea levels to war to worrying about being a bad friend, there are simply too many fears to fit in the day. We need a better way to fear. Join Jordan as he interviews experts about all the things that freak him out so he can decide whether he should be scared of this.