---
layout: work-item
title: Ukai Projects
client: Ukai Projects
role: Board member
date: 2023-05-14T20:52:08.476Z
categories: consulting
tags:
  - organizational_development
  - sector_development
thumbnail: /assets/uploads/ukai-11.jpg
images:
  - image: /assets/uploads/ukai-9.jpg
---
Co-founded and helped launch this cultural research and incubator non-profit; served on inaugural Board of Directors.

[UKAI Projects](https://www.ukai.ca/) helps people outside of traditional and institutional cultural production do their work and contribute to our collective well-being. We do this through research and prototyping with emerging technologies and tools, new ways of organizing to get things done, and new operating and business models. We employ counterfoil research, the role of culture in addressing social and environmental issues, and provide atypical, long-form residency opportunities for creative entrepreneurs.