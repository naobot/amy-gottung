---
layout: work-item
title: GreenPAC
client: Green PAC
client_link: https://www.greenpac.ca/
role: Consultant, grant writer
where: C﻿anada
date: 2023-05-10T20:23:00.202Z
categories: consulting
tags:
  - fundraising
thumbnail: /assets/uploads/greenpac3.jpeg
images:
  - image: /assets/uploads/greenpacscreen-shot-2022-01-02-at-6.00.00-pm.png
---
G﻿rant writing and development consulting for Canada's only non-partisan, non-profit working to build environmental leadership and government action on the environment.\
\
We do this through 3 programming streams:\
1 / PARLIAMENTARY INTERNSHIP FOR THE ENVIRONMENT\
\
Entering its 4th year, our Parliamentary Internship for the Environment places interns with MPs to support environmental leadership on Parliament Hill and helps prepare young Canadians for careers as\
environmental champions.\
\
2/ EVERY DAY ADVOCATES & 100 DEBATES\
\
We raise the capacity of Canadians across the nation to advocate for the environment and hold our elected officials accountable for their commitments.\
\
3/ CANDIDATE ENDORSEMENTS\
\
We identify environmental leaders of all major parties running for office and help connect voters and have run successful endorsement campaigns in the Federal (2015, 2019), Manitoba (2016), B.C. (2017) and Ontario (2018) elections.