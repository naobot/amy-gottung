---
layout: work-item
title: Queer Songbook Orchestra
date: 2023-05-14T23:29:18.979Z
categories: consulting
tags:
  - organizational_development
  - fundraising
thumbnail: /assets/uploads/qso-img_1961.jpeg
images:
  - image: /assets/uploads/qso-0y9a4362.jpeg
  - image: /assets/uploads/qso-nh-qso-18.jpeg
---
Strategic counsel through an ambitious period of growth, including launch of a cross-Canada tour and the development of an original [music video](http://www.queersongbook.com/short-film) and documentary project, via Canada Council’s New Chapter grant.