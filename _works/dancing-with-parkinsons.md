---
layout: work-item
title: Dancing with Parkinsons
link: ""
client: Dancing with Parkinsons
client_link: https://www.dancingwithparkinsons.com/
role: Consultant
when: "2023"
where: ""
date: 2024-10-31T16:49:28.343Z
categories: consulting
tags:
  - fundraising
  - business_development
thumbnail: /assets/uploads/screenshot-2024-10-29-at-8.51.25-pm.png
---
H﻿elped build strategy and secure e﻿ssential n﻿ational and regional funding (public and private foundations, government grants) for a﻿n ambitious national expansion.