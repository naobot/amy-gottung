---
layout: work-item
title: "Make 'em Laugh: The Funny Business of America"
client: PBS/WNET
client_link: ""
role: Production, Rights & Clearances, Research
when: 2007-2008
where: N﻿ew York, New York
team: Ghost Light Films, Thirteen / WNET (as Thirteen / WNET New York), Rhino
  Entertainment Company (in association with), BBC Cymru Wales (in association
  with), Public Broadcasting Service (PBS) (2009) (USA) (TV), Yleisradio (YLE)
  (2010) (Finland) (TV)
date: 2023-05-10T18:00:48.248Z
categories: creative
tags:
  - media
  - documentary
  - television
thumbnail: /assets/uploads/marx61oqgzhiz6l._ac_sl1000_.jpg
images:
  - image: /assets/uploads/richiepry.jpg
---
In core production team of five, delivered six-hour broadcast documentary series on the history of American comedy. Managed stills research, placement, licensing; prepared interviews.