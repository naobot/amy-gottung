---
layout: work-item
title: Facilitation
client: Various (incl:) Vibelab, Artscape Daniels Launchpad, Meridian Hall,
  University of Toronto
role: Facilitator
date: 2023-05-14T20:58:07.783Z
categories: speaking
tags:
  - facilitation
thumbnail: /assets/uploads/spkng327023965_10155914191798260_410529859374318782_o-2.jpeg
images:
  - image: /assets/uploads/danielsscreen-shot-2021-05-04-at-8.14.05-am.png
---
Workshop designer, facilitator

Recent projects include:

Alter-Places - Workshop Facilitator - "Strategies of Survival" - 2024 -with alternative cultural places, researchers (Sweden, Croatia, Canada, France, Germany, Ukraine) 

Vibelab - Lead Facilitator - 2023 public consultations to inform the City of Toronto's night economy strategy

VIVA Singers -Assistant Conductor - 2012-2022 - music education and performance program for youth and young adults with disabilities 

Artscape Daniels Launchpad - 2014-2020 - workshop facilitator (workshops for professional creatives). 

[Meridian Hall](http://www.meridianhall.com/) - 2015 - Specialist High Skills Major: Ontario high school program 

University of Toronto - 2012-2014 - Teaching Assistant (History of Western Music core courses)