---
layout: work-item
title: Nuville
link: https://nuville.net/
client: KRAK XR
client_link: ""
role: Project development, funding
when: "2021"
where: T﻿oronto, Canada
team: K﻿aren Vanderborght and team
partners: Museum of Toronto, Mammalian Diving Collective, Mandy Lam, Workman
  Arts, Cara Spooner, Tangled Art + Disability, et al.
date: 2023-05-10T17:38:18.267Z
categories: creative
tags:
  - media
  - project_development
thumbnail: /assets/uploads/screenshot-2023-05-10-at-10.42.07-am.png
---
In partnership with Creative Director Karen Vanderborght, generated funding (Canada Media Fund, Canada Council), exhibitions (Myseum of Toronto) and built an interdisciplinary team for the first phase of this experimental new media adventure.  

Nuville is a real world augmented audio game that turns your urban walk into a sci-fi adventure. Guided by audio instructions, players perform whimsical urban interventions which they record and share via accessible social media platforms. Nuville’s story and AR experience are a continuous collaboration between humans and AI technology.\
\
A “Gesamtkunstwerk” encompassing interactive performance, open AI, and urban design, the multiplayer sci-fi story is a casual XR game. Its creation, and first realizations, include collaborations with performance and parkour artists, sound artists, and a range of creative communities with unique urban perspectives.\
\
Nuville’s dystopian technocratic narrative is inspired by Jean-Luc Godard's nouvelle vague classic *Alphaville* and the manga series *Blame* by Tsutomu Nihei. Interactions are inspired by books by Alain De Botton (*Architecture of Happiness*), cultural theorist Paul Virillio (*City of Panic*) and architect Pierre Thibault (*Et si la beauté rendait heureux*).