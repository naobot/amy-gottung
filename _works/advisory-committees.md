---
layout: work-item
title: Advisory Committees
client: Various (Toronto Metropolitan University, City of Toronto, Urban Land
  Institute, et al.)
role: Committee member
date: 2023-05-14T23:25:21.650Z
categories: speaking
tags:
  - advisory
thumbnail: /assets/uploads/musicmusic-homepage-hero.jpeg
images:
  - image: /assets/uploads/musicloop-sessions-1.jpeg
---
**Advisory** \
\
City of Toronto - Nightlife Advisory Committee

University of Toronto School of Cities - Reimagining Music Venues

T﻿oronto Arts Council - Juror, Project Grants \
\
City of Toronto, Tourism - City of Toronto narrative focus group\
\
[Urban Land Institute](https://uli.org/) - Technical Assistance Panel - cultural space project\
\
City of Toronto Economic Recovery Group / Track - Music, Entertainment, Film\
\
[Ryerson FCAD Professional Music Program](https://www.ryerson.ca/music/) - program development committee