---
layout: work-item
title: Tapestry Opera
link: https://tapestryopera.com/
client: Tapestry New Opera
role: "Management role - fundraising (foundations, corporate, public) and marketing "
when: 2014-2016
where: T﻿oronto
team: M﻿ichael Mori, Katie Pounder, et al.
date: 2024-10-31T16:40:33.451Z
categories: consulting
tags:
  - fundraising
  - communications
  - management
  - development
thumbnail: /assets/uploads/screenshot-2024-10-31-at-9.47.51-am.png
images:
  - image: /assets/uploads/screenshot-2024-10-31-at-9.46.47-am.png
  - image: /assets/uploads/screenshot-2024-10-31-at-9.47.15-am.png
---
L﻿ed fundraising strategy for foundations (public and private), corporate giving and sponsorship, marketing and communications for Canada's largest contemporary opera company