---
layout: work-item
title: ReIssue - interview, audio piece
link: https://reissue.pub/articles/to-touch-the-ten-thousand-things-without-dependency-interlocking-wounds-and-intuitions-with-fan-wu-and-julian-hou/
client: ReIssue Magazine
client_link: https://reissue.pub/articles/to-touch-the-ten-thousand-things-without-dependency-interlocking-wounds-and-intuitions-with-fan-wu-and-julian-hou/
role: Producer, interviewer
when: "2023"
date: 2024-09-16T21:12:31.673Z
categories: creative
tags:
  - media
thumbnail: /assets/uploads/wuwuw.jpg
---
I﻿nterview with poet Fan Wu and artist Julian Hou

[L﻿ISTEN / READ](https://reissue.pub/articles/to-touch-the-ten-thousand-things-without-dependency-interlocking-wounds-and-intuitions-with-fan-wu-and-julian-hou/)