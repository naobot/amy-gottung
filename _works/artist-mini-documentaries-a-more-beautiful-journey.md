---
layout: work-item
title: "Mini-docs - neighbourhood spotlights: A More Beautiful Journey"
role: Producer/Director
when: "2023"
team: "DOP/Editor: Adam Seward | Tomorrow Night Films; PA/1st AC: Callum Kelley
  @vhsdeath8; AMBJ Marketing/Comms: Kat Cooper"
partners: K﻿at Cooper (communications)
date: 2023-05-31T14:50:44.281Z
categories: creative
tags:
  - media
  - documentary
thumbnail: /assets/uploads/screenshot-2023-05-31-at-10.54.54-am.png
---
Commissioned, p﻿roduced and co-directed three quick digital features on Toronto artists and their site-specific work of the 2023 AR sound project for public transit. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Cz69-R6TfkU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

O﻿BUXUM - Jane & Finch

<iframe width="560" height="315" src="https://www.youtube.com/embed/95oK81ROgTg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

R﻿ed Bear Singers - Regent Park

<iframe width="560" height="315" src="https://www.youtube.com/embed/7esyXxOFydQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

F﻿elipe Sena - Dufferin