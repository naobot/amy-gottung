---
layout: page
title: About
permalink: /about-old/
---
*what I do*

**creative / production** 

Independently and  collaboratively, I design, build, and resource cultural work (media, art, dialogue) - crafting custom architectures for custom projects.

My client and partner [roster](https://amygottung.com/Roster){:target="_blank"} includes avant studios, legacy institutions, peer-driven networks and sundry intrepid, independent teams.

**research / consulting**

I surface data, develop strategy, and open opportunities - for communities, sectors, and systems.

*practice*

* cultural programming, creative production (media, art, editorial)
* project and business development (grants, fundraising, partnerships)
* research and consulting

*approach*

* flexible and accessible
* analytic and detail-oriented
* equity focused 
* real (for real)

*values* 

My interests tend towards the marginal, relational, speculative, systemic, and complex. I’m propelled by the eternal persistence, and infinite configurations, of an alternative. 

![](/assets/uploads/dsf0417-copy.jpg)

*bio*

**Amy Gottung** is a cultural programmer/producer, writer, and researcher. Bridging sectors and disciplines, her practice is split between two principal streams.

As a consultant and fundraiser, Amy works with organizations in culture, media, and social-purpose spaces internationally. Her [client roster](https://amygottung.com/roster) embraces legacy institutions, service non-profits, avant production studios, and DIY collectives. Recent projects include a collaboration with [VibeLab,](https://vibe-lab.org/){:target="_blank"} a global leader in night culture, on consultations to inform the City of Toronto’s night economy strategy.

As a director/producer, Amy generates unique frameworks for boundary-pushing work. She has led landmark projects and diverse teams in the creation of experimental media, non-fiction television and video, international festivals, art-tech hackathons, and operas.

From 2016-2020 Amy served as the Executive Director of Toronto’s singular “anarchic, circus-like” alternative music and art series, [Long Winter](http://www.torontolongwinter.com/about){:target="_blank"} (Rolling Stone), founded in 2012. In four seasons, she helped to establish collective-based frameworks and a board of directors, introduced international collaborations with the co-production of a 2019 festival in Paris, France, tripled revenue and activity, and ushered the organization into annual operating funding streams. 

Amy is the creator of several cross-sector [interventions](http://www.torontolongwinter.com/diyspace){:target="_blank"} and [international exchanges](https://toronto.paris/){:target="_blank"} in support of alternative cultural scenes and spaces. Initiatives include a festival and research conference, co-presented by Long Winter (Toronto) and [La Station Gare des Mines](https://lastation.paris/){:target="_blank"} (Paris) in 2021, and the DIY Space Project: a multi-city, transdisciplinary research and advocacy program realized in partnership with [Trans Europe Halles](https://teh.net/){:target="_blank"} (a network of 100+ grassroots cultural centres) and leaders from government, real estate, and nightlife in Canada and Europe. 

She is a co-creator and creative producer of *[A More Beautiful Journey](https://amorebeautifuljourney.ca/){:target="_blank"},* a custom web augmented reality app that animated hundreds of kilometres of Toronto public transit line with original, spatialized music and sound from over 35 artists.

Amy holds a SSHRC-funded M.A. from the University of Toronto and a B. Mus from McGill University. She has lived and worked in Toronto, New York, Montréal, Sagueney (QC), Zürich, Massachusetts, Connecticut, and British Columbia. She speaks English, French, and basic German.

Amy collaborates and experiments with partners around the world.

*contact*

amy.gottung@gmail.com