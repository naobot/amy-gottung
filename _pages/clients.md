---
layout: columns
title: Client Roster
permalink: /clients
---
[VibeLab](https://vibe-lab.org/)

[2﻿21A](https://221a.ca/)

[The Laneway Project](https://www.thelanewayproject.ca/)

[Daily Bread](https://www.dailybread.ca/)

North York Food Cluster ([North York Community House](https://www.nych.ca/) & friends)

[Halocline Trance](https://haloclinetrance.bandcamp.com/)

[Trans Europe Halles](https://teh.net/)

[Collectif MU](http://www.mu.asso.fr/) / [La Station](https://lastation.paris/) (Fr)  

[The Stop Community Food Centre](http://thestop.org/)

[National Film Board](https://www.nfb.ca/)

[Space of Urgency](https://spaceofurgency.org/)

[S﻿econd Spring](https://secondspring.online/)

[Art Gallery of Ontario](https://ago.ca/)

[KRAK XR](https://www.krakxr.co/)

[Le Labo](http://lelabo.ca/en/)

[GreenPAC](https://www.greenpac.ca/)

[Long Winter Music and Arts Festival](http://www.torontolongwinter.com/)  

[Toronto Transit Commission](https://www.ttc.ca/)

[Music Gallery](https://musicgallery.org/)

[Level Justice](https://leveljustice.org/)

[Pride Toronto](https://www.pridetoronto.com/)

[Artscape Daniels Launchpad](http://www.artscapedanielslaunchpad.ca/)

[Harbourfront Centre](http://www.harbourfrontcentre.com/)

[D﻿uplex](https://www.instagram.com/duplexartistsociety/)

[S﻿arah Ruba](http://www.sarahruba.com/)

[City of Toronto](https://www.toronto.ca/)

[Jessy Lanza](https://jessylanza.com/)

[S﻿ylvain Sailly](http://www.zszsvzszs.com/)

[Radha Chaddah](https://www.radhachaddah.com/)

[Ryerson FCAD](https://www.ryerson.ca/fcad/)

[Sony Centre for the Performing Arts](http://www.sonycentre.ca/)

[L﻿il Sis](https://www.instagram.com/lilsis.ca/)

[D﻿ancing with Parkinson's](https://www.dancingwithparkinsons.com/)

[The Daniels Corporation](https://danielshomes.ca/)

[Luminato](https://luminatofestival.com/)

[Heritage Toronto](http://heritagetoronto.org/)

[University of Toronto](https://www.utoronto.ca/)

[The Kit Magazine](https://thekit.ca/)

[Queer Songbook Orchestra](http://www.queersongbook.com/)

[North York Harvest Food Bank](https://northyorkharvest.com/)

[Intersection Festival](http://www.intersectionfestival.org/)

[Paradigm Productions](https://www.susannafournier.com/paradigm-productions)

[Toronto Media Arts Centre](https://www.tomediaarts.org/)

[Intervene Design](https://www.intervene.space/)

[Myseum of Toronto](http://www.myseumoftoronto.com/)

[Tapestry Opera](https://tapestryopera.com/)

[Dynamic Maestro](https://www.cinesymphonyplanetearth.com/) / Johan de Meij (NY)

[MUTEK](http://www.mutek.org/en) (Mtl)

[Music in the Barns](http://www.musicinthebarns.com/)

[PBS/WNET](https://www.pbs.org/) (NY)

[Shared Path Consultation](https://sharedpath.ca/)

[Sweet Potato Chronicles](http://sweetpotatochronicles.com/)

[New Music Concerts](https://www.newmusicconcerts.com/)

[Ukai Projects](http://ukai.ca/)

[Airsa Professional Development for New Canadians in the Arts](http://airsa.org/)

[ChangeUp Art+Tech](http://changeup.io/)

[Dan Lim Photography](http://danlimphoto.com/)

[It's Not U It's Me](http://itsnotuits.me/)

[Opera on the Avalon](http://operaontheavalon.com/)

[Florentine Films](http://www.florentinefilms.com/) (NY)

[Sara Elgamal](https://sarathecamel.com/)

[Thin Edge New Music Collective](https://www.thethinedgenewmusiccollective.com/)

[New Music Concerts](https://www.newmusicconcerts.com/)

[Edwin Huizinga](http://www.edwinhuizinga.com/)

Leslie Ting / *[Speculation](https://www.speculationproduction.com/)*

[Chelsey Bennett](https://www.chelseybennett.com/)

[Andréa Tyniec](https://www.andreatyniec.ca/)

[VIVA Youth Singers of Toronto](https://www.vivayouthsingers.com/)

[LabEx (Sorbonne Nouvelle)](https://www.univ-spn.fr/portfolio/icca/)